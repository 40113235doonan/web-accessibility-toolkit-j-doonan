
'use strict';

//Increases text size
function textIncrease(e) {
  chrome.tabs.executeScript(null,
      {code:'document.documentElement.style.fontSize="110%", document.body.style.fontSize="110%"'});
}

document.addEventListener('DOMContentLoaded', function () {
  {
    $("#TI").click("click", textIncrease);
  }
});

//Reverts text size to normal
function textDecrease(e) {
  chrome.tabs.executeScript(null,
      {code:'document.body.style.fontSize="100%", document.documentElement.style.fontSize="100%"'});
}

document.addEventListener('DOMContentLoaded', function () {
  {
    $("#TD").click("click", textDecrease);
  }
});


//Increases page contrast dramatically
function highContrast(e) {
  chrome.tabs.executeScript(null,
      {code:'document.documentElement.style.WebkitFilter="contrast(999999999%)"'});
      $(this).one("click", highContrastOff);
}

//Reverts page back to normal
function highContrastOff(e) {
  chrome.tabs.executeScript(null,
      {code:'document.documentElement.style.WebkitFilter=""'});
      $(this).one("click", highContrast);
}

document.addEventListener('DOMContentLoaded', function () {
  {
    $("#HC").one("click", highContrast);
  }
});

//Increases page zoom
function zoomIncrease(e) {
  chrome.tabs.executeScript(null,
      {code:'document.body.style.zoom=1.3;this.blur();'});
}

document.addEventListener('DOMContentLoaded', function () {
  {
    $("#ZI").click("click", zoomIncrease);;
  }
});

//Zooms page back out to normal level
function zoomDecrease(e) {
  chrome.tabs.executeScript(null,
      {code:'document.body.style.zoom=1.0;this.blur();'});
}

document.addEventListener('DOMContentLoaded', function () {
  {
    $("#ZD").click("click", zoomDecrease);;
  }
});

//Inverts the colours of all page content
function invert(e) {
  chrome.tabs.executeScript(null,
      {code:'document.documentElement.style.WebkitFilter="invert(100%)"'});
      $(this).one("click", invertOff);
}

//Reverts page back to normal
function invertOff(e) {
  chrome.tabs.executeScript(null,
      {code:'document.documentElement.style.WebkitFilter=""'});
      $(this).one("click", invert);
}

document.addEventListener('DOMContentLoaded', function () {
  {
    $("#I").one("click", invert);;
  }
});

//Puts a greyscale filter over all page content
function blackWhite(e) {
  chrome.tabs.executeScript(null,
      {code:'document.documentElement.style.WebkitFilter="grayscale(100%)"'});
      $(this).one("click", blackWhiteOff);
}

//Reverts page back to normal
function blackWhiteOff(e) {
  chrome.tabs.executeScript(null,
      {code:'document.documentElement.style.WebkitFilter="grayscale(0%)"'});
      $(this).one("click", blackWhite);
}

document.addEventListener('DOMContentLoaded', function () {
  {
    $("#BW").one("click", blackWhite);;
  }
});

//Changes all text to Helvetica to make it more readable
function readableFonts(e) {
  chrome.tabs.executeScript(null,
      {code:'document.body.style.fontFamily="Helvetica", Times, serif;'});
      $(this).one("click", readableFontsOff);
}

//Reverts text back to original fonts
function readableFontsOff(e) {
  chrome.tabs.executeScript(null,
      {code:'document.body.style.fontFamily=""'});
      $(this).one("click", readableFonts);
}

document.addEventListener('DOMContentLoaded', function () {
  {
    $("#RF").one("click", readableFonts);;
  }
});

//Highlights all links on the page
function highlightLinks(e) {
  chrome.tabs.executeScript(null,
      {code:'var i;for(i=0;i<document.links.length;i++)document.links[i].style.backgroundColor="#31f5d1"'});
      $(this).one("click", highlightLinksOff);
}

//Unhighlights the links
function highlightLinksOff(e) {
  chrome.tabs.executeScript(null,
      {code:'var i;for(i=0;i<document.links.length;i++)document.links[i].style.backgroundColor=""'});
      $(this).one("click", highlightLinks);
}

document.addEventListener('DOMContentLoaded', function () {
  {
    $("#HL").one("click", highlightLinks);
  }
});

//Reads selected text aloud in a British Woman's voice
function textToSpeech(e) {
  chrome.tabs.executeScript( {
    code: "window.getSelection().toString();"}, function(selection) {
    chrome.tts.speak(selection[0], {'lang': 'en-GB', 'gender': 'female','rate': 1.0});
  });
}

document.addEventListener('DOMContentLoaded', function () {
  {
    $("#TTS").click("click", textToSpeech);
  }
});
